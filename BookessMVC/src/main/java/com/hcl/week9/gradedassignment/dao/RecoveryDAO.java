package com.hcl.week9.gradedassignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;



public class RecoveryDAO {
	
	@Autowired
	private JdbcTemplate template;

	
	public boolean changePassword(String username, String password) {
		String sql = "update BookUsers set password=? where username=? ";
		int i = template.update(sql, password, username);
		if (i!=0)
			return true;
		else
			return false;
	}
//
//		try {
//			PreparedStatement ps = con.prepareStatement("update BookUsers set password=? where username=? ");
//			ps.setString(2, username);
//			ps.setString(1, password);
//			if (ps.executeUpdate() != 0)
//				return true;
//			else
//				return false;
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return false;
//		}
//
//	}
}

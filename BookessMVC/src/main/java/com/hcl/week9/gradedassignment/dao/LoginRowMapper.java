package com.hcl.week9.gradedassignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.hcl.week9.gradedassignment.beans.BookUsers;

public class LoginRowMapper implements RowMapper {

	@Override
	public BookUsers mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		BookUsers users = new BookUsers();
		users.setUsername(rs.getString("username"));
		users.setPassword(rs.getString("password"));
		return users;
	}

}

package com.hcl.week9.gradedassignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;


import com.hcl.week9.gradedassignment.beans.ReadLaterBooks;

public class ReadLaterDAO {
	
	@Autowired
	private JdbcTemplate template;
	

	public boolean addBooks(int id, String name, String genre) throws DuplicateKeyException {
		String sql = "insert into ReadLater values(?,?,?)";
		int i  = template.update(sql, id , name , genre);
		if (i!=0)
			return true;
		else
			throw new DuplicateKeyException("Book Already Exists");
	}
		
		public List<ReadLaterBooks> getAllBooks() {
			String sql = "select id, name , genre from ReadLater";
			return template.query(sql, new ReadLaterRowMapper());
		}
	


	public boolean deleteBook(int id) {
		String sql = "delete from ReadLater where id = ?";
		int i = template.update(sql, id);
		if(i!=0)
			return true;
		else
			return false;
	}


//
//	public boolean addBooks(int id, String name, String genre) {
//		String sql = "insert into ReadLater values(?,?,?)";
//		PreparedStatement ps;
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setInt(1, id);
//			ps.setString(2, name);
//			ps.setString(3, genre);
//			ps.executeUpdate();
//			ps.close();
//
//			return true;
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//
//	}
//
//	public List<ReadLaterBooks> getAllBooks() {
//		List<ReadLaterBooks> list = new ArrayList<ReadLaterBooks>();
//
//		try {
//			PreparedStatement ps = con.prepareStatement("select id, name , genre from ReadLater ");
//
//			boolean result = ps.execute();
//			if (result == true) {
//				ResultSet rs = ps.executeQuery();
//				while (rs.next()) {
//					ReadLaterBooks books = new ReadLaterBooks();
//					books.setId(rs.getInt("id"));
//					books.setName(rs.getString("name"));
//					books.setGenre(rs.getString("genre"));
//					list.add(books);
//
//				}
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
//
//	public boolean deleteBook(int id) {
//		String sql = "delete from ReadLater where id = ?";
//		PreparedStatement ps;
//		try {
//			ps = con.prepareStatement(sql);
//			ps.setInt(1, id);
//			ps.executeUpdate();
//			ps.close();
//
//			return true;
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//
//	}
}

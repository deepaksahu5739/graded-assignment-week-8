package com.hcl.week9.gradedassignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.week9.gradedassignment.beans.Books;
import com.hcl.week9.gradedassignment.dao.BooksDAO;

public class BooksService {

	@Autowired
	BooksDAO dao;

	public List<Books> getAllBooks() {
		return dao.getAllBooks();

	}
}

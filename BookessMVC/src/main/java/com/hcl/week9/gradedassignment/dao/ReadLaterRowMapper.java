package com.hcl.week9.gradedassignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.hcl.week9.gradedassignment.beans.ReadLaterBooks;

public class ReadLaterRowMapper implements RowMapper {

	@Override
	public ReadLaterBooks mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		ReadLaterBooks books = new ReadLaterBooks();
		books.setId(rs.getInt("id"));
		books.setName(rs.getString("name"));
		books.setGenre(rs.getString("genre"));
		return books;
	}

}

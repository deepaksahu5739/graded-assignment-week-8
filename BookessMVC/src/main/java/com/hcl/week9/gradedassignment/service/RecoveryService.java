package com.hcl.week9.gradedassignment.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.week9.gradedassignment.dao.RecoveryDAO;

public class RecoveryService {

	@Autowired
	RecoveryDAO dao;

	public boolean changePassword(String username, String password) {
		return dao.changePassword(username, password);
	}
}

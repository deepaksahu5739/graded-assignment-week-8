package com.hcl.week9.gradedassignment.controller;

import java.util.List;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.hcl.week9.gradedassignment.beans.LikedBooks;
import com.hcl.week9.gradedassignment.exception.UserException;
import com.hcl.week9.gradedassignment.service.BooksService;
import com.hcl.week9.gradedassignment.service.LikedBooksService;
import com.hcl.week9.gradedassignment.service.LoginService;
import com.hcl.week9.gradedassignment.service.ReadLaterService;
import com.hcl.week9.gradedassignment.service.RecoveryService;
import com.hcl.week9.gradedassignment.service.RegisterService;

@Controller
public class BookController {

	@Autowired
	RegisterService service;
	
	@Autowired
	LoginService service1;
	
	@Autowired
	RecoveryService service2;
	
	@Autowired
	BooksService service3;
	
	@Autowired
	LikedBooksService service4;
	
	@Autowired
	ReadLaterService service5;
	
	@RequestMapping("/login")  
	public String login()
	{
		return "login";
	}
	
	@RequestMapping("/register")  
	public String register()
	{
		return "register";
	}
	
	@RequestMapping("/change")  
	public String changePassword()
	{
		return "change";
	}
	
	@RequestMapping("/home")  
	
    public ModelAndView home()  
    {  
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home");
		mv.addObject("books",service3.getAllBooks());
		System.out.println("success");
		System.out.println(service3.getAllBooks());
        return mv;  
    } 
	
	@PostMapping("/register")
	public ModelAndView registerUser(@RequestParam("username") String username, @RequestParam("password") String password) {

		try {
			service.registerUser(username, password);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("registerSuccess");
			mv.addObject("username",username);
			return mv;
		} catch (DuplicateKeyException e) {
			// TODO Auto-generated catch block
			ModelAndView mv = new ModelAndView();
			mv.setViewName("registerError");
			return mv;
		}
		

	}
	
	
	@PostMapping("/liked")  
    public ModelAndView Likedlist(@RequestParam("id") int id,@RequestParam("name") String name,
			@RequestParam("genre") String genre)  
    {  
		try
		{
			service4.addBooks(id, name, genre);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("liked");
			System.out.println("Added to Liked List");
			mv.addObject("books",service4.getAllBooks());
			System.out.println(service4.getAllBooks());
			return mv; 
		}
		catch(DuplicateKeyException e)
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("likedError");
			return mv;
		}
			
         
    }
	
	
	
	
	@PostMapping("/readLater")  
    public ModelAndView readLater(@RequestParam("id") int id,@RequestParam("name") String name,
			@RequestParam("genre") String genre)  
    {  
		try
		{
			service5.addBooks(id, name, genre);
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLater");
			System.out.println("Added to Read Later List");
			mv.addObject("books",service5.getAllBooks());
			System.out.println(service5.getAllBooks());
			return mv; 
		}
		catch(DuplicateKeyException e)
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLaterError");
			return mv;
		}
			
         
    }
	
	@GetMapping("/readLater")  
    public ModelAndView readLaterAfterDelete()  
    {  
		
			ModelAndView mv = new ModelAndView();
			mv.setViewName("readLater");
			mv.addObject("books",service5.getAllBooks());
			System.out.println(service5.getAllBooks());
			return mv; 
			
         
    }
	@GetMapping("/liked")  
    public ModelAndView LikedAfterDelete()  
    {  
		
			ModelAndView mv = new ModelAndView();
			mv.setViewName("liked");
			mv.addObject("books",service4.getAllBooks());
			System.out.println(service4.getAllBooks());
			return mv; 
			
         
    }
	
	
	@PostMapping("/login")
	public ModelAndView validateUser(@RequestParam("username") String username,
			@RequestParam("password") String password) {

		if (service1.validateUser(username, password))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("successlogin");
			mv.addObject("books",service3.getAllBooks());
			System.out.println("Success");
			System.out.println(service3.getAllBooks());
	        return mv; 
		}
		else {
			ModelAndView mv = new ModelAndView();
			mv.setViewName("loginError");
			return mv;
		}
			

	}
	
	@PostMapping("/change")
	public ModelAndView changePassword(@RequestParam("username") String username,
			@RequestParam("password") String password) {

		if (service2.changePassword(username, password))
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("changeSuccess");
			mv.addObject("username",username);
			return mv;
			
		}
		else
		{
			ModelAndView mv = new ModelAndView();
			mv.setViewName("changeFailure");
			return mv;
		}

	}
	
	@GetMapping("/readLaterDelete/{id}")
	public String readLaterDelete(@PathVariable("id") int id) {

		service5.deleteBook(id);

		return "redirect:/readLater";
	}
	
	@GetMapping("/likedDelete/{id}")
	public String likedDelete(@PathVariable("id") int id) {

		service4.deleteBook(id);

		return "redirect:/liked";
	}
	
	
	
	
}

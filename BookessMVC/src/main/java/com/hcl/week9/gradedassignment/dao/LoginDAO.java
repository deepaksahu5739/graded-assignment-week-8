package com.hcl.week9.gradedassignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.week9.gradedassignment.beans.BookUsers;



public class LoginDAO {
	
	
	@Autowired
	private JdbcTemplate template;



	public boolean validateUser(String username, String password) {
		String sql = "select username, password from BookUsers where username=? and password=?";
		List<BookUsers> user = template.query(sql, new LoginRowMapper(), username , password);
		if(user.size()==0)
			return false;
		else
			return true;
		
	}
//	boolean status = false;
//
//	public boolean validateUser(String username, String password) {
//
//		try {
//			System.out.println(con);
//			PreparedStatement ps = con
//					.prepareStatement("select username, password from BookUsers where username=? and password=?");
//			ps.setString(1, username);
//			ps.setString(2, password);
//
//			ResultSet rs = ps.executeQuery();
//			status = rs.next();
//			return status;
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return status;
//		}
//
//	}
}

package com.hcl.week9.gradedassignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.week9.gradedassignment.beans.Books;

public class BooksDAO {

	@Autowired
	private JdbcTemplate template;


	public List<Books> getAllBooks() {
		
		String sql = "select id, name , genre from Books";
		return template.query(sql, new BookRowMapper());
		
	}
//		List<Books> list = new ArrayList<Books>();
//
//		try {
//			PreparedStatement ps = con.prepareStatement("select id, name , genre from Books ");
//
//			boolean result = ps.execute();
//			if (result == true) {
//				ResultSet rs = ps.executeQuery();
//				while (rs.next()) {
//					Books books = new Books();
//					books.setId(rs.getInt("id"));
//					books.setName(rs.getString("name"));
//					books.setGenre(rs.getString("genre"));
//					list.add(books);
//
//				}
//			}
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return list;
//	}
}

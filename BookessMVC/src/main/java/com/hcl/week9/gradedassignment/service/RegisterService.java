package com.hcl.week9.gradedassignment.service;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.hcl.week9.gradedassignment.dao.RegisterDAO;
import com.hcl.week9.gradedassignment.exception.UserException;

public class RegisterService {

	@Autowired
	RegisterDAO dao ;

	public boolean registerUser(String username, String password) throws DuplicateKeyException {
		return dao.registerUser(username, password);
	}
}

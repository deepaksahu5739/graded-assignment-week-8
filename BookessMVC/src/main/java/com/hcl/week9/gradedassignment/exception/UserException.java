package com.hcl.week9.gradedassignment.exception;

public class UserException extends Exception {

	private String msg;

	public UserException(String msg) {
		this.msg = msg;
	}
	
}

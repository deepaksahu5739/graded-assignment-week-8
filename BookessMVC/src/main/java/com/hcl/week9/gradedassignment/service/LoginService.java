package com.hcl.week9.gradedassignment.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.hcl.week9.gradedassignment.dao.LoginDAO;

public class LoginService {

	@Autowired
	LoginDAO dao ;

	public boolean validateUser(String username, String password) {
		return dao.validateUser(username, password);
	}
}

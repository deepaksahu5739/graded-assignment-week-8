package com.hcl.week9.gradedassignment.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


import com.hcl.week9.gradedassignment.beans.Books;
import com.hcl.week9.gradedassignment.beans.LikedBooks;
import com.hcl.week9.gradedassignment.beans.ReadLaterBooks;
import com.hcl.week9.gradedassignment.dao.BooksDAO;
import com.hcl.week9.gradedassignment.dao.LikedDAO;
import com.hcl.week9.gradedassignment.dao.LoginDAO;
import com.hcl.week9.gradedassignment.dao.ReadLaterDAO;
import com.hcl.week9.gradedassignment.dao.RecoveryDAO;
import com.hcl.week9.gradedassignment.dao.RegisterDAO;
import com.hcl.week9.gradedassignment.service.BooksService;
import com.hcl.week9.gradedassignment.service.LikedBooksService;
import com.hcl.week9.gradedassignment.service.LoginService;
import com.hcl.week9.gradedassignment.service.ReadLaterService;
import com.hcl.week9.gradedassignment.service.RecoveryService;
import com.hcl.week9.gradedassignment.service.RegisterService;

@Configuration
public class AppConfig {

	@Bean
	@Scope(value = "prototype")
	public Books book() {

		return new Books();
	}
	
	@Bean
	@Scope(value = "prototype")
	public LikedBooks likedBook() {

		return new LikedBooks();
	}
	
	@Bean
	@Scope(value = "prototype")
	public ReadLaterBooks readLaterBook() {

		return new ReadLaterBooks();
	}
	
	@Bean
	public RegisterService service() {

		return new RegisterService();
	}
	
	@Bean
	public LoginService service1() {

		return new LoginService();
	}
	
	@Bean
	public RecoveryService service2() {

		return new RecoveryService();
	}
	
	@Bean
	public BooksService service3() {

		return new BooksService();
	}
	
	@Bean
	public LikedBooksService service4() {

		return new LikedBooksService();
	}
	
	@Bean
	public ReadLaterService service5() {

		return new ReadLaterService();
	}
	
	@Bean
	public BooksDAO dao() {

		return new BooksDAO();
	}
	
	@Bean
	public LikedDAO likedDao() {

		return new LikedDAO();
	}
	
	@Bean
	public LoginDAO loginDao() {

		return new LoginDAO();
	}
	
	@Bean
	public RecoveryDAO recoveryDao() {

		return new RecoveryDAO();
	}
	
	@Bean
	public ReadLaterDAO readLaterDao() {

		return new ReadLaterDAO();
	}
	
	@Bean
	public RegisterDAO registerDao() {

		return new RegisterDAO();
	}
	
	
	
	
	@Bean
	public DriverManagerDataSource ds() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	DriverManagerDataSource ds = new DriverManagerDataSource();
    	ds.setUrl("jdbc:mysql://localhost:3306/webbooks");
    	ds.setUsername("root");
    	ds.setPassword("*Yy8179175989");
    	
    	return ds;
    	
    }
    @Bean
    public JdbcTemplate template() {
    	
    	JdbcTemplate template = new JdbcTemplate();
    	template.setDataSource(ds());
    	return template;
    	
    }
}

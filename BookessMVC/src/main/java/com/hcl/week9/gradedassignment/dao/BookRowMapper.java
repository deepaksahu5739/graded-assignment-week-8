package com.hcl.week9.gradedassignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import com.hcl.week9.gradedassignment.beans.Books;

public class BookRowMapper implements RowMapper {

	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Books books = new Books();
		books.setId(rs.getInt("id"));
		books.setName(rs.getString("name"));
		books.setGenre(rs.getString("genre"));
		return books;
	}

}

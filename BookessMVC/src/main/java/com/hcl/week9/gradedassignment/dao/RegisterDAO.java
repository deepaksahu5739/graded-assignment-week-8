package com.hcl.week9.gradedassignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;


import com.hcl.week9.gradedassignment.exception.UserException;



public class RegisterDAO {

	@Autowired
	private JdbcTemplate template;


	public boolean registerUser(String username, String password) throws DuplicateKeyException{
		
		String sql = "insert into BookUsers values (?,?)";
		 if(template.update(sql, username, password)!=0)
			return true;
		else
			throw new DuplicateKeyException("Username Already Exists.! try with different Username");
		
		
	}
//
//		try {
//
//			String sql = "insert into BookUsers values (?,?)";
//			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setString(1, username);
//			ps.setString(2, password);
//			ps.executeUpdate();
//			ps.close();
//
//			return true;
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//	}

}